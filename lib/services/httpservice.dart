import 'package:dio/dio.dart';
import 'package:first_app/models/package.dart';

class HttpService {
  Dio dio = Dio();
  String url;
  String token = "invalidtoken";

  HttpService({required this.url}) {
    dio.options.baseUrl = url;
    dio.options.receiveTimeout = 3000;
    dio.options.connectTimeout = 5000;
  }

  errorHandler(dynamic error) {
    if (error is DioError) {
      if (error.response != null) {
        switch (error.response?.statusCode) {
          case 401:
            throw "unauthorised access";
        }
      } else {
        throw "no response";
      }
    } else {
      throw "unknown error";
    }
  }

  Future<String> login(String username, String password) async {
    try {
      var response = await dio
          .post('/login', data: {"username": username, "password": password});

      token = response.data["token"];
      return token;
    } catch (error) {
      try {
        errorHandler(error);
      } catch (e) {
        return "error";
      }

      return "";
    }
  }

  Future<List<Package>> getPackages(int limit, int page) async {
    try {
      final params = <String, int>{"_limit": limit, "_page": page};
      dio.options.queryParameters = Map.of(params);
      var response = await dio.get('/posts');
      List<Package> packageList = response.data
          .map<Package>((data) => Package.fromJson(data))
          .toList() as List<Package>;
      return packageList;
    } catch (error) {
      try {
        errorHandler(error);
      } catch (e) {
        return [];
      }
      return [];
    }
  }
}
