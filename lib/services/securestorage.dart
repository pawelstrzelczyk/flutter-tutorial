import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SecureStorageService {
  final storage = const FlutterSecureStorage();

  getValue(String key) async {
    String? value = await storage.read(key: key);
    return value;
  }

  putValue(String key, String value) async {
    await storage.write(key: key, value: value);
  }

  deleteValue(String key) async {
    await storage.delete(key: key);
  }
}
