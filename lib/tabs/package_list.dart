import 'package:first_app/components/packagelistview.dart';
import 'package:first_app/providers/login.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';

class PackageListPage extends StatelessWidget {
  const PackageListPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    ToastContext().init(context);
    return Scaffold(
      appBar: AppBar(
          title: const Text("Package List"),
          leading: const Icon(Icons.question_mark),
          actions: [
            Consumer<LoginProvider>(
              builder: (context, login, child) {
                return IconButton(
                    onPressed: () {
                      login.logout("token");
                      Navigator.pop(context);
                      Toast.show("logged out",
                          duration: Toast.lengthLong, gravity: Toast.bottom);
                    },
                    icon: const Icon(Icons.logout));
              },
            )
          ]),
      body: const PackageListView(),
      // Center(
      //   child: SizedBox(
      //     height: MediaQuery.of(context).size.height * 0.8,
      //     width: MediaQuery.of(context).size.width,
      //     //crossAxisAlignment: CrossAxisAlignment.center,
      //     child: const PackageListView(),
      //   ),
      // ),
    );
  }
}
