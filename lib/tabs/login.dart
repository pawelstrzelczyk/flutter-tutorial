import 'package:first_app/components/submitemailfield.dart';
import 'package:first_app/components/submitpasswordfield.dart';
import 'package:first_app/providers/login.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> with TickerProviderStateMixin {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late AnimationController animationController;
  bool logged = false;

  // @override
  // void dispose() {
  //   animationController.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    ToastContext().init(context);
    return Scaffold(
      appBar: AppBar(
          title: const Text("Guess the number"),
          leading: const Icon(Icons.email)),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(40.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    Consumer<LoginProvider>(
                      builder: (context, login, child) {
                        return const SubmitEmailField();
                      },
                    ),
                    Consumer<LoginProvider>(
                      builder: (context, login, child) {
                        return const SubmitPasswordField();
                      },
                    ),
                    const Padding(
                      padding: EdgeInsets.only(top: 20.0),
                    ),
                    Consumer<LoginProvider>(
                      builder: (context, login, child) {
                        return !(login.loginStatus == LoginStatus.logging)
                            ? ElevatedButton(
                                onPressed: () async {
                                  if (_formKey.currentState!.validate()) {
                                    await login.login();
                                    if (login.loginStatus ==
                                        LoginStatus.failure) {
                                      Toast.show("logging error");
                                      login.loginController.clear();
                                      login.passwordController.clear();
                                      setState(() {
                                        FocusManager.instance.primaryFocus
                                            ?.unfocus();
                                        Navigator.of(context)
                                            .pushNamedAndRemoveUntil(
                                                "/", (route) => false);
                                      });
                                    } else if (login.loginStatus ==
                                        LoginStatus.success) {
                                      setState(() {
                                        FocusManager.instance.primaryFocus
                                            ?.unfocus();
                                        Navigator.of(context)
                                            .pushNamed("/package");
                                      });
                                    }
                                  }
                                },
                                style: ButtonStyle(
                                    fixedSize: MaterialStateProperty.all<Size>(
                                      Size(
                                          MediaQuery.of(context).size.width *
                                              0.8,
                                          48),
                                    ),
                                    shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(16.0),
                                            side: const BorderSide(
                                                color: Colors.deepOrange)))),
                                child: const Text(
                                  "LOGIN",
                                  style: TextStyle(fontSize: 16),
                                ),
                              )
                            : const Center(
                                child: CircularProgressIndicator(),
                              );
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
