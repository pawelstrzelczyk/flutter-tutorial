import 'package:first_app/models/package.dart';
import 'package:flutter/material.dart';

class PackageDetailsPage extends StatelessWidget {
  const PackageDetailsPage({
    Key? key,
    required this.package,
  }) : super(key: key);
  final Package package;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${package.id} Package Details"),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(Icons.arrow_back_outlined),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          children: [
            Text(
              package.title,
              style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            Text(package.body),
          ],
        ),
      ),
    );
  }
}
