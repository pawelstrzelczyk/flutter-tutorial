import 'package:first_app/models/package.dart';
import 'package:first_app/services/httpservice.dart';
import 'package:flutter/material.dart';

enum FetchState { idle, fetching, fetched }

class PackageProvider extends ChangeNotifier {
  FetchState fetchState = FetchState.idle;
  HttpService packageHttpService =
      HttpService(url: "https://jsonplaceholder.typicode.com/");

  List<Package> packageList = [];
  int limit = 10;
  int page = 1;

  fetchPackages() async {
    fetchState = FetchState.fetching;
    notifyListeners();

    try {
      var data = await packageHttpService.getPackages(limit, page);
      packageList.addAll(data);
      await Future.delayed(const Duration(seconds: 2), () {});
      notifyListeners();
    } catch (e) {
      print(e);
    }
    fetchState = FetchState.fetched;
    notifyListeners();
  }
}
