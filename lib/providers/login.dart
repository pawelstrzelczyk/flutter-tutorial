import 'package:first_app/services/httpservice.dart';
import 'package:first_app/services/securestorage.dart';
import 'package:flutter/material.dart';

enum LoginStatus { success, failure, logging, signedOut }

class LoginProvider extends ChangeNotifier {
  late LoginStatus loginStatus = LoginStatus.signedOut;
  final loginController = TextEditingController();
  final passwordController = TextEditingController();
  SecureStorageService secureStorageService = SecureStorageService();
  String token = "";
  String username = "";
  String? password;
  HttpService loginHttpService = HttpService(url: 'http://192.168.1.217:3000');
  logout(String key) async {
    secureStorageService.deleteValue(key);
    loginController.clear();
    passwordController.clear();
    loginStatus = LoginStatus.signedOut;
  }

  login() async {
    try {
      loginStatus = LoginStatus.logging;
      notifyListeners();
      username = loginController.text.toString();
      password = passwordController.text.toString();
      token = await loginHttpService.login(username, password!);
      if (token == "error") {
        loginStatus = LoginStatus.failure;
        notifyListeners();
        return "error";
      } else {
        loginStatus = LoginStatus.success;
        secureStorageService.putValue("token", token);
        notifyListeners();
        return null;
      }
    } catch (error) {
      rethrow;
    }
  }
}
