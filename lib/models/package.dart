class Package {
  final int id;
  final int userId;
  final String title;
  final String body;
  Package({this.id = -1, this.userId = -1, this.title = "", this.body = ""});

  factory Package.fromJson(Map<String, dynamic> json) {
    return Package(
        id: json["id"],
        userId: json["userId"],
        title: json["title"],
        body: json["body"]);
  }
}
