import 'package:first_app/providers/login.dart';
import 'package:first_app/providers/package.dart';
import 'package:first_app/tabs/package_list.dart';
import 'package:first_app/tabs/login.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<PackageProvider>(
          create: ((_) => PackageProvider()),
        ),
        ChangeNotifierProvider<LoginProvider>(
          create: ((_) => LoginProvider()),
        )
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Package List',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        bottomAppBarColor: Colors.black,
        primarySwatch: Colors.deepOrange,
      ),
      initialRoute: "/",
      routes: <String, WidgetBuilder>{
        "/": (_) => const LoginPage(),
        "/package": (_) => const PackageListPage(),
      },
      debugShowCheckedModeBanner: false,
    );
  }
}
