import 'package:first_app/providers/login.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SubmitEmailField extends StatefulWidget {
  const SubmitEmailField({Key? key, String? labelFor}) : super(key: key);
  @override
  State<StatefulWidget> createState() => SubmitEmailFieldState();
}

class SubmitEmailFieldState extends State<SubmitEmailField> {
  final TextEditingController controller = TextEditingController();
  String labelFor = "e-mail";
  String hintText = "e-mail";
  FocusNode focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    focusNode.addListener(() {
      focusNode.hasFocus ? hintText = '' : hintText = "e-mail";
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LoginProvider>(builder: (context, login, child) {
      return TextFormField(
        focusNode: focusNode,
        validator: (value) {
          String? val;
          val = value.toString();
          if (val.isEmpty) {
            return "Please provide $labelFor";
          } else if (val.length <= 2) {
            return "$labelFor is too short";
          }
          return null;
        },
        controller: login.loginController,
        keyboardType: TextInputType.visiblePassword,
        textAlign: TextAlign.center,
        cursorColor: Colors.black,
        decoration: InputDecoration(
            helperText: labelFor,
            suffixIcon: const Icon(Icons.email),
            hintText: hintText),
      );
    });
  }
}
