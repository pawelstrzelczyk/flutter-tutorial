import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SubmitNumberTextField extends StatelessWidget {
  final TextEditingController controller;
  final String labelFor;

  const SubmitNumberTextField(
      {Key? key, required this.controller, required this.labelFor})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: (value) {
        int? val;
        val = int.tryParse(value.toString());
        if (val == null) {
          return "Please provide some number";
        } else if (val > 20 || val < 0) {
          return "Number not in range";
        }
        return null;
      },
      controller: controller,
      keyboardType: TextInputType.number,
      inputFormatters: [FilteringTextInputFormatter.allow(RegExp(r'[0-9]+'))],
      textAlign: TextAlign.center,
      cursorColor: Colors.black,
      decoration: InputDecoration(
        helperText: labelFor,
      ),
    );
  }
}
