import 'package:first_app/models/package.dart';
import 'package:first_app/providers/package.dart';
import 'package:first_app/tabs/package_details.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PackageListView extends StatefulWidget {
  const PackageListView({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => PackageListViewState();
}

class PackageListViewState extends State<PackageListView> {
  late ScrollController scrollController;

  onScrollUpdated() async {
    if (scrollController.offset >= scrollController.position.maxScrollExtent &&
        !scrollController.position.outOfRange) {
      Provider.of<PackageProvider>(context, listen: false).page += 1;
      await Provider.of<PackageProvider>(context, listen: false)
          .fetchPackages();
    }
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    Provider.of<PackageProvider>(context, listen: false).limit = 10;
    Provider.of<PackageProvider>(context, listen: false).page = 1;
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<PackageProvider>(context, listen: false).fetchPackages());
    scrollController = ScrollController();
    scrollController.addListener(() {
      onScrollUpdated();
    });
  }

  Widget buildList() {
    return Consumer<PackageProvider>(
      builder: (context, package, child) {
        return RefreshIndicator(
          child: ListView.separated(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            physics: const AlwaysScrollableScrollPhysics(),
            controller: scrollController,
            itemBuilder: (context, index) {
              if (index < package.packageList.length) {
                return ListTile(
                  title: Text(package.packageList[index].title),
                  subtitle: Text(package.packageList[index].body),
                  leading: Text(package.packageList[index].id.toString()),
                  tileColor: Colors.amber[200],
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(
                      builder: (context) {
                        return PackageDetailsPage(
                          package: package.packageList[index],
                        );
                      },
                    ));
                  },
                );
              } else {
                return Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  child: package.fetchState == FetchState.fetching
                      ? const Center(
                          child: const CircularProgressIndicator(),
                        )
                      : const Center(
                          child: const Text("No more data."),
                        ),
                );
              }
            },
            separatorBuilder: (context, _) {
              return const SizedBox(
                height: 16,
              );
            },
            itemCount: package.packageList.length + 1,
          ),
          onRefresh: () async {
            package.fetchPackages();
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return buildList();
  }
}
