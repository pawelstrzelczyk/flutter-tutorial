import 'package:first_app/providers/login.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SubmitPasswordField extends StatefulWidget {
  const SubmitPasswordField({Key? key, String? labelFor}) : super(key: key);
  @override
  State<StatefulWidget> createState() => SubmitPasswordFieldState();
}

class SubmitPasswordFieldState extends State<SubmitPasswordField> {
  final TextEditingController controller = TextEditingController();
  final String labelFor = "password";
  bool _isObscure = true;
  String hintText = "password";
  FocusNode focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    focusNode.addListener(() {
      focusNode.hasFocus ? hintText = '' : hintText = "password";
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LoginProvider>(builder: (context, login, child) {
      return TextFormField(
        focusNode: focusNode,
        validator: (value) {
          String? val;
          val = value.toString();
          if (val.isEmpty) {
            return "Please provide $labelFor";
          } else if (val.length <= 2) {
            return "$labelFor is too short";
          }
          return null;
        },
        controller: login.passwordController,
        keyboardType: TextInputType.visiblePassword,
        textAlign: TextAlign.center,
        cursorColor: Colors.black,
        decoration: InputDecoration(
            helperText: labelFor,
            suffixIcon: IconButton(
                onPressed: () {
                  setState(() {
                    _isObscure = !_isObscure;
                  });
                },
                icon: _isObscure
                    ? const Icon(Icons.visibility_off)
                    : const Icon(Icons.visibility)),
            hintText: hintText),
        obscureText: _isObscure,
      );
    });
  }
}
